package pokemon;

import java.io.*;
import java.util.*;

public class pokemon {

	public static final String POKEFILE = "Pokemon.csv";

	public static void main(String[] args) throws IOException {
		BufferedReader in = null;
		BufferedWriter out = null;
		String line;
		String typeKeyboard;

		try {

			System.out.println("What's your pokemon type?");
			Scanner input = new Scanner(System.in);
			typeKeyboard = input.nextLine();
			input.close();

			in = new BufferedReader(new FileReader(POKEFILE));
			out = new BufferedWriter(new FileWriter(typeKeyboard));

			while ((line = in.readLine()) != null) {
				Scanner scan = new Scanner(line);
				scan.useDelimiter(",");
				scan.next();
				scan.next();

				if (scan.next().equalsIgnoreCase(typeKeyboard) || (scan.next().equalsIgnoreCase(typeKeyboard))) {
					out.write(line);
					out.write("\n");
				}
				scan.close();
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}
}
